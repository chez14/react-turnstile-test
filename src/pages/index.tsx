import { Turnstile } from '@marsidev/react-turnstile'
import classNames from 'classnames'
import { useId, useState } from 'react'
import { Col, Container, Row, ToggleButton, ToggleButtonGroup } from 'react-bootstrap'

import styles from "@/styles/Home.module.scss";

const TURNSTILE_KEYS = {
  interactive: "3x00000000000000000000FF",
  noninteractive: "1x00000000000000000000AA",
  invisible: "0x4AAAAAAAB_GbAxK5CsP0SL",
  invisible_forced: "0x4AAAAAAAB_GbAxK5CsP0SL"
}


export default function Home() {

  const turnstile = useId()
  const [turnstileSiteKey, setTurnstileSiteKey] = useState<keyof typeof TURNSTILE_KEYS>("noninteractive")


  function handleKeySelect(val: keyof typeof TURNSTILE_KEYS) {
    setTurnstileSiteKey(val)
  }
  return (
    <>
      <div className="my-5">
        <Container>
          <Row className='justify-content-center'>
            <Col md={8}>
              <div className={classNames(['my-5', styles.markEverything])}>
                <h3>React Turnstile Test</h3>
                <p className="lead">Active SiteKey: <code>{TURNSTILE_KEYS[turnstileSiteKey]}</code></p>
                <div className={classNames(styles.turnstileContainer, { [styles.forced]: turnstileSiteKey === "invisible_forced" })}>
                  <Turnstile siteKey={TURNSTILE_KEYS[turnstileSiteKey]} id={turnstile} options={{ theme: 'dark' }} />
                </div>
              </div>

              <div className="my-5">
                <ToggleButtonGroup type="radio" name="options" defaultValue={'noninteractive'} onChange={(val) => handleKeySelect(val)}>
                  <ToggleButton id="interative" value={'interactive'}>
                    Interactive
                  </ToggleButton>
                  <ToggleButton id="noninterative" value={'noninteractive'}>
                    Non-interactive
                  </ToggleButton>
                  <ToggleButton id="invisible" value={'invisible'}>
                    Invisible (Prod key)
                  </ToggleButton>
                  <ToggleButton id="invisible_forced" value={'invisible_forced'}>
                    Invisible (Prod key + force resize)
                  </ToggleButton>
                </ToggleButtonGroup>
              </div>

              <div className="my-5">
                <h4>Context</h4>
                <p>
                  See the code on <a href="https://gitlab.com/chez14/react-turnstile-test">https://gitlab.com/chez14/react-turnstile-test</a>.
                </p>
                <p>Installed <code>@marsidev/react-turnstile</code>: v0.0.5.</p>
              </div>
            </Col>
          </Row>
        </Container>
      </div>
    </>
  )
}
